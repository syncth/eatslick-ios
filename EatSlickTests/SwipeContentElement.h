//
//  ADSwipeContentElement.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SwipeContentElement <NSObject>
@property (strong) IBOutlet UIViewController * leftViewController;
@end