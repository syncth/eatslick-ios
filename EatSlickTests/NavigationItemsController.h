//
//  ADLeftViewController.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NavigationItemsController : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *header;
@end
