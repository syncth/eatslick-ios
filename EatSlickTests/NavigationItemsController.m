//
//  ADLeftViewController.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "NavigationItemsController.h"
#import "SwipeContentViewController.h"

@interface NavigationItemsController ()

@end

@implementation NavigationItemsController
@synthesize header;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setHeader:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
    [(SwipeContentViewController *)self.parentViewController backToMain];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.header;
}
@end
