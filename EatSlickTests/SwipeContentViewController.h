//
//  ADSwipeContentViewController.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeContentElement.h"

enum SwipeContentViewMode {
    ADSwipeContentViewModeMain = 0,
    ADSwipeContentViewModeRight = 1,
    ADSwipeContentViewModeLeft = 2
};

@interface SwipeContentViewController : UIViewController
@property IBOutlet UIViewController * leftViewController;
@property IBOutlet UIViewController * rightViewController;
@property IBOutlet UIViewController * mainViewController;

@property IBOutlet UIView * leftView;
@property IBOutlet UIView * rightView;
@property IBOutlet UIView * mainView;

@property enum SwipeContentViewMode currentMode;
-(void)toggleLeftView;
-(void)presentRightView;
-(void)backToMain;
@end