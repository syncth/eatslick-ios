//
//  ADServiceConfigTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ADServiceConfigTest.h"
#import "ServiceConfig.h"
#import "ServerConfig.h"
#import "ADAppDelegate.h"


@implementation ADServiceConfigTest


-(void)testShouldCheckServiceURL{
    ServiceConfig * serviceConfig = [[ServiceConfig alloc] initWithServerConfig:[ServerConfig new]];
    
    NSURL * pathForRestaurents= [serviceConfig urlForServiceWithKey:API_RESTAURENTS_KEY];

    NSDictionary * serverConfDict=[NSDictionary dictionaryWithContentsOfURL:[[NSBundle mainBundle] URLForResource:SERVER_CONF_FILE withExtension:@"plist"]];
    
    NSDictionary * serviceConfDict=[NSDictionary dictionaryWithContentsOfURL:[[NSBundle mainBundle] URLForResource:SERVICE_CONF_FILE withExtension:@"plist"]];
    
    
    NSDictionary * restaurants_dict=[serviceConfDict objectForKey:API_RESTAURENTS_KEY];
    
    NSString * expectedString=[NSString stringWithFormat:@"%@://%@:%@/%@", [serverConfDict objectForKey:SERVER_SCHEMA_KEY], [serverConfDict objectForKey:SERVER_ADD_KEY], [serverConfDict objectForKey:SERVER_PORT_KEY], [restaurants_dict objectForKey:@"path"]];
    
    STAssertTrue([pathForRestaurents.absoluteString isEqualToString:expectedString],@"%@!=%@", pathForRestaurents.absoluteString, expectedString);
    
}


-(void)testShouldCheckServiceType{
    ServiceConfig * serviceConfig = [[ServiceConfig alloc] initWithServerConfig:[ServerConfig new]];
    enum RequestMethodType typeOfRequestForRestaurents= [serviceConfig methodTypeForServiceWithKey:API_RESTAURENTS_KEY];
    STAssertTrue(typeOfRequestForRestaurents==RequestTypeGET, @"should be get");
}


@end
