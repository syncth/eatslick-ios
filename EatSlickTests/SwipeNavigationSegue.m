//
//  ADSwipeNavigationSegue.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 26/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "SwipeNavigationSegue.h"
#import "SwipeContentViewController.h"
#import "SwipeContentElement.h"
#import "UIViewController-swipeSegue.h"


@implementation SwipeNavigationSegue

-(void)perform{
        if ([self.identifier isEqualToString:SET_MAIN_VIEWS]) {
            SwipeContentViewController * sourceController=self.sourceViewController;
            UIViewController<SwipeContentElement> * destinationController=self.destinationViewController;
            sourceController.mainViewController=destinationController;
            return;
    }
    if ([self.identifier isEqualToString:SET_LEFT_VIEWS]) {
        SwipeContentViewController * sourceController=self.sourceViewController;
        UIViewController<SwipeContentElement> * destinationController=self.destinationViewController;
        sourceController.leftViewController=destinationController;
    }
}

@end