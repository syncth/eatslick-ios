//
//  ADServerConfTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ADServerConfTest.h"
#import "ServerConfig.h"

@implementation ADServerConfTest

-(void)testShouldCheckServerConfig{
    NSURL * fileURL=[[NSBundle mainBundle] URLForResource:@"serverConf" withExtension:@"plist"];
    NSDictionary * dict=[[NSDictionary alloc] initWithContentsOfURL:fileURL];
    ServerConfig * conf=[[ServerConfig alloc] init];
    STAssertTrue([[dict objectForKey:SERVER_ADD_KEY] isEqualToString:conf.serverAddress], @"test should give proper server config");
    STAssertTrue([[dict objectForKey:SERVER_PORT_KEY] isEqualToString:conf.serverPort], @"test should give proper server config");
    STAssertTrue([[dict objectForKey:SERVER_SCHEMA_KEY] isEqualToString:conf.schema], @"test should give proper server config");
}

@end