//
//  ServiceConfig.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerConfig.h"


enum RequestMethodType {
    RequestTypeGET = 1,
    RequestTypePOST = 2,
    RequestTypePOSTJSON=3
};

@interface ServiceConfig : NSObject
-(id)initWithServerConfig:(ServerConfig *)config;
-(NSURL *)urlForServiceWithKey:(NSString *)string;
-(enum RequestMethodType)methodTypeForServiceWithKey:(NSString *)key;
@end