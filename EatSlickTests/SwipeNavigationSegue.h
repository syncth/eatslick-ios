//
//  ADSwipeNavigationSegue.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 26/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SET_MAIN_VIEWS @"setContent"
#define SET_LEFT_VIEWS @"setLeftView"

@interface SwipeNavigationSegue : UIStoryboardSegue
@end