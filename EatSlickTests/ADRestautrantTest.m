//
//  ADRestautrantTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ADRestautrantTest.h"
#import "Restaurant.h"
#import "ADAppDelegate.h"

@interface ADRestautrantTest ()
@property NSManagedObjectContext * context;
@property NSEntityDescription *entity;
@end

@implementation ADRestautrantTest
@synthesize context, entity;

-(void)setUp{
    self.context= [[ADAppDelegate instance] managedObjectContext];
    self.entity = [NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:context];
}

-(void)tearDown{
    [Restaurant clearAll];
}

-(void)testShouldCreateRestaurentFromDict{
    NSData * data=[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"parsing_test" withExtension:@"json"]];
    NSError * error;
    NSArray *restaurents = [NSJSONSerialization JSONObjectWithData:data options:NSJSONWritingPrettyPrinted error:&error];
    NSDictionary * dict =[restaurents objectAtIndex:0];
    Restaurant * restaurant=[Restaurant fromDict:dict];
    STAssertTrue([restaurant.name isEqualToString:@"13th floor"],@"should be same as in doc");
}


-(void)testShouldSaveRestaurantsFromServer{
    NSData * dictFromServer=[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"parsing_test" withExtension:@"json"]];
    NSError * error;
    NSArray *restaurents = [NSJSONSerialization JSONObjectWithData:dictFromServer options:NSJSONWritingPrettyPrinted error:&error];
    NSDictionary * dict =[restaurents objectAtIndex:0];
    Restaurant * restaurant=[Restaurant fromDict:dict];
    [restaurant save];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];   
    [request setEntity:self.entity];
    NSArray *fetchResults=[self.context executeFetchRequest:request error:&error];
    STAssertNil(error, @"should not be an error");
    STAssertTrue(fetchResults.count!=0 ,@"number of results should not be 0");
    NSManagedObject * obj=[fetchResults objectAtIndex:0];
    STAssertTrue(obj==restaurant, @"should be equal to saved object");
}

@end