//
//  ServiceConfig.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ServiceConfig.h"

@interface ServiceConfig()
@property (strong) NSDictionary * serviceDict;
@property (strong) ServerConfig * serverConfig;
@end


@implementation ServiceConfig
@synthesize serviceDict=_serviceDict;
@synthesize serverConfig=_serverConfig;

-(id)initWithServerConfig:(ServerConfig *)config{
    self=[super init];
    if (self) {
        NSURL * filePath=[[NSBundle mainBundle] URLForResource:@"serviceConfig" withExtension:@"plist"];
        self.serviceDict=[[NSDictionary alloc] initWithContentsOfURL:filePath];
        self.serverConfig=config;
    }
    return self;
}

-(NSURL *)urlForServiceWithKey:(NSString *)key{
    NSDictionary * dict=[self.serviceDict objectForKey:key];
    NSString * urlString=[NSString stringWithFormat:@"%@://%@:%@/%@", self.serverConfig.schema, self.serverConfig.serverAddress, self.serverConfig.serverPort, [dict objectForKey:@"path"]];
    return [NSURL URLWithString:urlString];
}

-(enum RequestMethodType)methodTypeForServiceWithKey:(NSString *)key{
    NSDictionary * dict=[self.serviceDict objectForKey:key];
    if ([[dict objectForKey:@"type"] isEqualToString:@"GET"]) {
        return RequestTypeGET;
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"POST"]) {
        return RequestTypePOST;
    }
    else {
        return RequestTypePOSTJSON;
    }
}

@end