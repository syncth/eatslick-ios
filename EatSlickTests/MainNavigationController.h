//
//  ADMainViewController.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeContentElement.h"

@interface MainNavigationController : UINavigationController<SwipeContentElement>
@end