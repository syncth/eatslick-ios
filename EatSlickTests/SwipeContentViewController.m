//
//  ADSwipeContentViewController.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 27/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "SwipeContentViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SwipeNavigationSegue.h"
#import "UIViewController-swipeSegue.h"

@interface SwipeContentViewController ()
@end

@implementation SwipeContentViewController
@synthesize leftView, rightView, mainView, currentMode, leftViewController=_leftViewController, rightViewController=_rightViewController, mainViewController=_mainViewController;

-(void)setLeftViewController:(UIViewController *)leftViewController{
    _leftViewController=leftViewController;
    leftViewController.swipeContentController=self;
    [self.leftView addSubview:leftViewController.view];
}

-(UIViewController *)leftViewController{
    return _leftViewController;
}

-(void)setRightViewController:(UIViewController *)rightViewController{
    _rightViewController=rightViewController;
    rightViewController.swipeContentController=self;
    [self.rightView addSubview:rightViewController.view];
}

-(UIViewController *)rightViewController{
    return _rightViewController;
}

-(void)setMainViewController:(UIViewController *)mainViewController{
    _mainViewController=mainViewController;
    mainViewController.swipeContentController=self;
    [self.mainView addSubview:mainViewController.view];
}

-(UIViewController *)mainViewController{
    return _mainViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSegueWithIdentifier:SET_MAIN_VIEWS sender:self];
    [self performSegueWithIdentifier:SET_LEFT_VIEWS sender:self];
    self.mainView.layer.shadowOffset = CGSizeMake(0, 0);
    self.mainView.layer.shadowRadius = 5;
    self.mainView.layer.shadowOpacity = 0.5;
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)toggleLeftView{
    if (self.currentMode==ADSwipeContentViewModeLeft) {
        [self backToMain];
        return;
    }
    [self.leftView removeFromSuperview];
    [self.view insertSubview:self.leftView belowSubview:self.mainView];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect currentFrame=self.mainView.frame;
        self.mainView.frame= CGRectMake(260, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height);
        self.currentMode=ADSwipeContentViewModeLeft;
    }];
}

-(void)presentRightView{
    [self.rightView removeFromSuperview];
    [self.view insertSubview:self.rightView belowSubview:self.mainView];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect currentFrame=self.mainView.frame;
        self.mainView.frame= CGRectMake(-260, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height);
        self.currentMode=ADSwipeContentViewModeRight;
    }];
}

-(void)backToMain{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect currentFrame=self.mainView.frame;
        self.mainView.frame= CGRectMake(0, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height);
        self.currentMode=ADSwipeContentViewModeMain;
    }];
}

-(void)setMainFrameTo:(CGRect)frame{
    self.mainView.frame= frame;
}

-(BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers{
    return YES;
}

@end
