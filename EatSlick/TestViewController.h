//
//  TestViewController.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (unsafe_unretained) IBOutlet UITableView * tableView;
@end
