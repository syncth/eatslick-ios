//
//  ADConnectionHandler.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 30/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ServerConfig.h"
#import "ConnectionHandler.h"
#import "NSURLConnection-block.h"


@interface ServerConnection : NSObject
-(id)initWithServerConfig:(ServerConfig *)conf;
-(void)asyncRequestForServiceKey:(NSString *)key andParams:(NSDictionary *)params success:(void(^)(NSData *,NSURLResponse *))successBlock_ failure:(void(^)(NSData *,NSError *))failureBlock_;
@end