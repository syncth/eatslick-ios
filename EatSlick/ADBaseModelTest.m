//
//  BaseModelTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ADBaseModelTest.h"
#import "BaseModel.h"
#import "ADAppDelegate.h"

@implementation ADBaseModelTest

-(void)testShouldClearAllModelsOfGivenEntity{
    [BaseModel clearAllModelsWithEntity:@"Restaurant"];
    NSManagedObjectContext * context= [[ADAppDelegate instance] managedObjectContext];
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:context];
    NSFetchRequest * request= [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError * error;
    NSArray * results= [context executeFetchRequest:request error:&error];
    STAssertNil(error, @"should not be errors");
    STAssertTrue(results.count==0,@"result count should be 0");
}

@end