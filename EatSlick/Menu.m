//
//  Menu.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "Menu.h"
#import "Restaurant.h"
#import "Special.h"


@implementation Menu

@dynamic items;
@dynamic restaurant;
@dynamic combos;
@dynamic specials;

@end
