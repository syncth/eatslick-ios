//
//  BGStreachableButton.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 10/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "BGStreachableButton.h"

@implementation BGStreachableButton

-(void)awakeFromNib{
    UIImage * image=[self backgroundImageForState:UIControlStateNormal];
    UIImage *stretchableButtonImage = [image stretchableImageWithLeftCapWidth:8 topCapHeight:0];
    [self setBackgroundImage:stretchableButtonImage forState:UIControlStateNormal];

    image=[self backgroundImageForState:UIControlStateNormal];
    stretchableButtonImage = [image stretchableImageWithLeftCapWidth:8 topCapHeight:0];
    [self setBackgroundImage:stretchableButtonImage forState:UIControlStateHighlighted];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
