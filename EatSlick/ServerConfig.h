//
//  ADServerConfig.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 30/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SERVER_ADD_KEY @"serverAddress"
#define SERVER_PORT_KEY @"serverPort"
#define SERVER_SCHEMA_KEY @"schema"

@interface ServerConfig : NSObject
@property (strong) NSString * serverAddress;
@property (strong) NSString * serverPort;
@property (strong) NSString * schema;
@end
