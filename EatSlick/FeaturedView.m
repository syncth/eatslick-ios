//
//  FeaturedView.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 11/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "FeaturedView.h"

@implementation FeaturedView

-(void)awakeFromNib{
    [self setContentSize:CGSizeMake(self.frame.size.width*2, self.frame.size.height)];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
