//
//  ADServerConfig.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 30/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//


#import "ServerConfig.h"

@implementation ServerConfig
@synthesize serverPort, serverAddress, schema;

-(id)init{
    self=[super init];
    if (self) {
        NSDictionary * config=[NSDictionary dictionaryWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"serverConf" withExtension:@"plist"]];
        self.serverAddress=[config objectForKey:SERVER_ADD_KEY];
        self.serverPort=[config objectForKey:SERVER_PORT_KEY];
        self.schema=[config objectForKey:SERVER_SCHEMA_KEY];
    }
    return self;
}

@end
