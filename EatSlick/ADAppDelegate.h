//
//  ADAppDelegate.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 23/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ServerConnection.h"
#import "ServiceConfig.h"

@interface ADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong) ServerConnection * connection;
@property (strong) ServiceConfig * serviceConfig;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
+(ADAppDelegate *)instance;
@end
