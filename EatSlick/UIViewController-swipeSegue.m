//
//  UIViewController-swipeSegue.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "UIViewController-swipeSegue.h"
#import "SwipeContentViewController.h"

@implementation UIViewController(swipeSegue)

-(SwipeContentViewController *)swipeContentController{
    if (!self.parentViewController) {
        return nil;
    }
    if ([self.parentViewController isKindOfClass:[SwipeContentViewController class]]) {
        return (SwipeContentViewController *)self.parentViewController;
    }
    else {
        return self.parentViewController.swipeContentController;
    }
}

-(void)setSwipeContentController:(SwipeContentViewController *)swipeContenrController{
    [swipeContenrController addChildViewController:self];
}

@end