//
//  UIViewController-swipeSegue.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeContentViewController.h"

@class SwipeContentViewController;

@interface UIViewController(swipeSegue)
@property (unsafe_unretained) IBOutlet SwipeContentViewController * swipeContentController;
@end
