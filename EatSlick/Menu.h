//
//  Menu.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Restaurant, Special;

@interface Menu : NSManagedObject

@property (nonatomic, retain) NSManagedObject *items;
@property (nonatomic, retain) Restaurant *restaurant;
@property (nonatomic, retain) NSManagedObject *combos;
@property (nonatomic, retain) Special *specials;

@end