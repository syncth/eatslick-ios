//
//  BaseModel.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface BaseModel : NSManagedObject
+(BOOL)clearAllModelsWithEntity:(NSString *)entityName;
@end
