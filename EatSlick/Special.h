//
//  Special.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Special : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSManagedObject *menu;
@property (nonatomic, retain) NSManagedObject *item;

@end
