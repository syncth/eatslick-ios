//
//  ParsingBlocks.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ParsingBlocks.h"
#import "Restaurant.h"

@implementation ParsingBlocks
@synthesize restaurantsParser;

-(id)init{
    self=[super init];
    if (self) {
        self.restaurantsParser=(^id(NSData * data, NSURLResponse * response){
            NSError * error;
            NSArray *restaurents = [NSJSONSerialization JSONObjectWithData:data options:NSJSONWritingPrettyPrinted error:&error];
            NSMutableArray * array=[NSMutableArray new];
            for (NSDictionary * dict in restaurents) {
                [array addObject:[Restaurant fromDict:dict]];
            }
            return array;
        });
    }
    return self;
}


@end
