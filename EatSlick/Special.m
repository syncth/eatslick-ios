//
//  Special.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "Special.h"


@implementation Special

@dynamic title;
@dynamic menu;
@dynamic item;

@end
