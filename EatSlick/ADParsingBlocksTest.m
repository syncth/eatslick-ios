//
//  ADParsingBlocksTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//
//

#import "ADParsingBlocksTest.h"
#import "ParsingBlocks.h"
#import "Restaurant.h"
#import "ADAppDelegate.h"

@implementation ADParsingBlocksTest

-(void)tearDown{
    [Restaurant clearAll];
}


-(void)testShouldCheckParsingBlocks{
    NSData * data=[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"parsing_test" withExtension:@"json"]];
    ParsingBlocks * blocks= [ParsingBlocks new];
    NSArray * restaurents= blocks.restaurantsParser(data, nil);
    STAssertTrue(restaurents.count==1,@"only one restaurent is available for the test");
    STAssertTrue([[(Restaurant *)[restaurents objectAtIndex:0] name] isEqualToString:@"13th floor"],@"name should be same as in sample data");
}

@end
