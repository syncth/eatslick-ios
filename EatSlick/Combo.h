//
//  Combo.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Menu;

@interface Combo : NSManagedObject

@property (nonatomic, retain) Menu *menu;

@end
