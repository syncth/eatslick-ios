//
//  Restaurant.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 01/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//
#import "Restaurant.h"
#import "ServerConnection.h"
#import "ADAppDelegate.h"
#import "BaseModel.h"

#define NAME_KEY @"name"

@implementation Restaurant
@dynamic name;
@dynamic desc;

+(Restaurant *)fromDict:(NSDictionary *)dict{
    NSManagedObjectContext * context=[[ADAppDelegate instance] managedObjectContext];
    Restaurant * obj= [NSEntityDescription insertNewObjectForEntityForName:@"Restaurant" inManagedObjectContext:context];
    obj.name=[dict objectForKey:@"name"];
    return obj;
}

-(void)destroy{
    NSManagedObjectContext * context=[[ADAppDelegate instance] managedObjectContext];
    [context deleteObject:self];
}

-(void)fetchFeaturedRestaurants{
//    ADConnection * connection= [[ADAppDelegate instance] connection];
}

-(BOOL)save{
    return false;
}

+(BOOL)clearAll{
    return [BaseModel clearAllModelsWithEntity:@"Restaurant"];
}

@end