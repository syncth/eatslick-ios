//
//  ADConnectionTest.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 01/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.

#import "ADConnectionTest.h"
#import "ServerConnection.h"
#import "ADAppDelegate.h"
#import "NSURLConnection-block.h"

@interface ADConnectionTest()
@end

@implementation ADConnectionTest

-(void)atestShouldCheckServiceConnection{
    ADAppDelegate * appDelegate=[ADAppDelegate instance];
    ServerConnection * connection=[appDelegate connection];
    __block BOOL flagged=false;
    [connection asyncRequestForServiceKey:API_RESTAURENTS_KEY andParams:nil success:^(NSData * data, NSURLResponse *response){
        flagged=TRUE;
    } failure:^(NSData * data, NSError * error){
        flagged=TRUE;
    }];
    NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:4];
    while ([loopUntil timeIntervalSinceNow] > 0){
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:loopUntil];
    }
    STAssertTrue(flagged, @"should connect to server");
}

@end
