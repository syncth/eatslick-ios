//
//  main.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 23/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ADAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ADAppDelegate class]));
    }
}
