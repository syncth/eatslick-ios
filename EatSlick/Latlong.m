//
//  Latlong.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "Latlong.h"
#import "Restaurant.h"


@implementation Latlong

@dynamic latitude;
@dynamic longitude;
@dynamic restaurant;

@end
