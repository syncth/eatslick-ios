//
//  ADConnectionHandler.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 30/06/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "ServerConnection.h"
#import "ConnectionHandler.h"
#import "SerializableObject.h"
#import "ADAppDelegate.h"
#import "ParsingBlocks.h"

@interface ServerConnection()
@property (strong) ServerConfig * config;
@end

@implementation ServerConnection
@synthesize config;

-(id)initWithServerConfig:(ServerConfig *)conf{
    self=[super init];
    if (self) {
        self.config=conf;
    }
    return self;
}

-(void)asyncRequestForServiceKey:(NSString *)key andParams:(NSDictionary *)params success:(void(^)(NSData *,NSURLResponse *))successBlock_ failure:(void(^)(NSData *,NSError *))failureBlock_{
    ServiceConfig * conf=[[ADAppDelegate instance] serviceConfig];
    NSURL * url=[conf urlForServiceWithKey:key];
    NSURLRequest * request=[NSURLRequest requestWithURL:url];
    [NSURLConnection asyncRequest:request success:[successBlock_ copy] failure:[failureBlock_ copy]];
}

@end