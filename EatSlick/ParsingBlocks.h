//
//  ParsingBlocks.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id(^JsonParser)(NSData *,NSURLResponse *);

@interface ParsingBlocks : NSObject
@property (nonatomic, copy) JsonParser restaurantsParser;
@end
