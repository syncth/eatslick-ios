//
//  Latlong.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Restaurant;

@interface Latlong : NSManagedObject

@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) Restaurant *restaurant;

@end
