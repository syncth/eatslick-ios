//
//  FeaturedItemView.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 10/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "FeaturedItemView.h"

@implementation FeaturedItemView

-(void)awakeFromNib{
    self.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"featured_bg"]];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
