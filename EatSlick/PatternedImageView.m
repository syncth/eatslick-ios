//
//  PatternedImageView.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 10/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "PatternedImageView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PatternedImageView

-(void)awakeFromNib{
    UIImage * image=[UIImage imageNamed:@"bg_gap"];
    self.backgroundColor=[UIColor colorWithPatternImage:image];
    [self setOpaque:NO];
    self.layer.opaque=NO;
}

@end
