//
//  BaseModel.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "BaseModel.h"
#import "ADAppDelegate.h"

@implementation BaseModel

+(BOOL)clearAllModelsWithEntity:(NSString *)entityName{
    NSManagedObjectContext * context= [[ADAppDelegate instance] managedObjectContext];
    NSEntityDescription * entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError * error;
    NSArray *fetchResults=[context executeFetchRequest:request error:&error];
    for (NSManagedObject * obj in fetchResults) {
        [context deleteObject:obj];
    }
    return error==nil;
}

@end