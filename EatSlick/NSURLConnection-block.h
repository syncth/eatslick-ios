//
//  NSURLConnection-block.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 01/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLConnection(block)
+ (void)asyncRequest:(NSURLRequest *)request success:(void(^)(NSData *,NSURLResponse *))successBlock_ failure:(void(^)(NSData *,NSError *))failureBlock_;
@end
