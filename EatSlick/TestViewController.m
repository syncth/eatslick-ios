//
//  TestViewController.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#define SHOW_SEARCH @"showSearch"

#import "TestViewController.h"
#import "CustomTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface TestViewController ()
@property (strong)NSMutableDictionary * selectedTimes;
@property (strong)NSIndexPath * lastSelectedPath;
@end

@implementation TestViewController
@synthesize selectedTimes, lastSelectedPath, tableView;

-(void)awakeFromNib{
    self.selectedTimes=[NSMutableDictionary new];
}

- (IBAction)showSearch:(id)sender {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    if(self.lastSelectedPath){
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
        
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)tableView:(UITableView *)ktableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    self.lastSelectedPath=indexPath;
//    CustomTableViewCell * cell= (CustomTableViewCell *)[ktableView cellForRowAtIndexPath:indexPath];
//    [cell.bg.layer removeAllAnimations];
//    cell.bg.alpha=1;
//    [cell.bg setNeedsDisplay];
}

-(NSString *)stringOfIndexPath:(NSIndexPath *)path{
    return [NSString stringWithFormat:@"%i-%i", path.row, path.section];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)ktableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomTableViewCell * cell=  [ktableView dequeueReusableCellWithIdentifier:indexPath.row%2==0?@"test1":@"test2"];
    return cell;
}

@end