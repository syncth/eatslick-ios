//
//  SwipeSegue.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TOGGLE_LEFT_VIEW @"toggleLeftView"

@interface SwipeSegue : UIStoryboardSegue

@end
