//
//  ADConnectionHandler.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 01/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerConnection.h"

@class ServerConnection;

@protocol ConnectionHandler <NSObject>
@required 
-(void)connection:(ServerConnection *)connection hasFetchedData:(NSData *)data forRequest:(NSURLRequest *)request;
-(void)connection:(ServerConnection *)connection hasFailedWithError:(NSError *)error forRequest:(NSURLRequest *)request;
@end