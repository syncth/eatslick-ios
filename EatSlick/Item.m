//
//  Item.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "Item.h"
#import "Menu.h"
#import "Special.h"


@implementation Item

@dynamic name;
@dynamic menu;
@dynamic special;

@end
