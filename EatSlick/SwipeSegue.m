//
//  SwipeSegue.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 07/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "SwipeSegue.h"
#import "SwipeContentViewController.h"

@implementation SwipeSegue

-(void)perform{
    if ([self.identifier isEqualToString:TOGGLE_LEFT_VIEW]) {
        UIViewController * _sourceViewController= self.sourceViewController;
        [(SwipeContentViewController *)_sourceViewController.navigationController.parentViewController toggleLeftView];
    }
}



@end
