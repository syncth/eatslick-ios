//
//  CustomTableViewCell.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 08/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (unsafe_unretained) IBOutlet UIView * bg;
@end
