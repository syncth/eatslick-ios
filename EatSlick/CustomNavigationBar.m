//
//  CustomNavigationBar.m
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 09/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//

#import "CustomNavigationBar.h"

@implementation CustomNavigationBar

-(void)awakeFromNib{
    [self setBackgroundImage:[UIImage imageNamed:@"bg_title_bar.png"] forBarMetrics:UIBarMetricsDefault];
    self.backgroundColor=[UIColor blackColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
