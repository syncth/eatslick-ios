//
//  Restaurant.h
//  EatSlick
//
//  Created by Thirumalasetti Karthik on 01/07/12.
//  Copyright (c) 2012 AlphaDevs. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseModel.h"

@interface Restaurant : BaseModel
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
+(Restaurant *)fromDict:(NSDictionary *)dict;
-(BOOL)save;
+(BOOL)clearAll;
@end